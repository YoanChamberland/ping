import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.scss']
})
export class PostListComponentComponent implements OnInit {
  title = 'mon premier post';
  content = 'test';
  loveIts = 0;
  // tslint:disable-next-line: variable-name
  @Input() created_at = new Promise((resolve, reject) => {
    const date = new Date();
    setTimeout(() => {
      resolve(date);
    }, 2);
  });
  decremente() {
    this.loveIts -= 1;
  }

  incremente() {
    this.loveIts += 1;
  }

  constructor() {}

  ngOnInit() {}
}
