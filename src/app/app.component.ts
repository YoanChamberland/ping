import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Post';
  posts: [
    {
      title: 'Mon premier post';
      content: 'test';
      loveIts: 0;
    }
  ];
}
